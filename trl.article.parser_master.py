#                                           RISKIQ                                                                   
#                                    **THREATRESEARCHLABS##%                                                           
#                                **(######################%%%%%%                                                       
#                             ((((((#####################%%%%%%%%%#                                                    
#                           ((((((((####*               #%%%%%%%%####                                                  
#                         (((((((((/*                       %%%%#####(/                                                
#                        (((((((/**   ,////((((((((((((###*   %#####((//                                               
#                       ((((((***%%%%%%%%%%#(((((((((((#########(/#((////                                              
#                      ,((((****#%%%%%%%%%%%%%%%%%###((############//////,                                             
#                     %((((****/##%%%%%%%%%(,       /#################/////                                            
#                   %%%(((/////(###                           ((########/////                                          
#                 %%%%%((//////                                   */###%%(((///                                        
#               (%%%%%%(///////, _____________________.____       **/%%%((((((/                                                    
#              %%%%%%%%%///////, \__    ___/\______   \    |      ////(%%(((((((                                                   
#             %%%%%%%%% ///////,,  |    |    |       _/    |      (((((, %#(((((((                                                  
#             %%%%%%%%   ///////,  |    |    |    |   \    |___   ###(,   %(((((((                                                  
#             %%%%%%%,    ((((((,  |____|    |____|_  /_______ \  %%*,,   %((((((*                                                  
#            %%%%%%%,,     (((((*,                  \/        \/ #%#*,    %((((((,                                      
#             ###%%%,,      (((((,,,,                       ####*****.     ((((((,,                                    
#             ######,,,      (((((,,,,*                   ####******      ((((((,,,                                    
#              #####,,,,      .(((/,,,***               ###********      (((((/,,,                                     
#               ####/,,,*.      (((/,,****/*         /(/*********       ((((/,,,,                                      
#                (((#,,,****      ((/,****////*   ((***********      ((((/*****,                                       
#                  ((((,*****/*,    .(****/////*************     *///*********                                         
#                    (((/****//////,,,,,****************(/******************                                           
#                       (((**//////////(((((((((((((((*******************                                              
#                           ((/////////(((((((((((,,,,,,,************
#

from newspaper import Article
import newspaper
from newspaper import Config, Article, Source
from newspaper.article import ArticleException
from markdownify import markdownify
import nltk
import html2markdown
nltk.download('punkt')
import uuid
import os
import re
from pathlib import Path
from urllib.parse import urlparse
import urllib.parse
from urllib.parse import unquote
from bs4 import BeautifulSoup

import iocx_nltk
import iocx_otx
import iocx
#from trlparser import *
references = '/Users/unkn0wn/Repositories/trl.article.parser/references'

urls = [url.strip() for url in open(references).readlines() if url]
base = os.getcwd()

for i, url in enumerate(urls):
    try:
        article = Article(url, keep_article_html=True)
        article.download()
        article.parse()
        article.nlp()
        with open('HTML/'+'RiskIQ.'+(article.title).replace('?', '').replace('/', ' ')+'.html'.format(i), 'w') as f:
            f.write(article.article_html) 
        md = markdownify(article.article_html)
        with open('HTML/'+'RiskIQ.'+(article.title).replace('?', '').replace('/', ' ')+'.full.html'.format(i), 'w') as ar:
            ar.write(article.html)
        artauth= open('HTML/'+'RiskIQ.'+(article.title).replace('?', '').replace('/', ' ')+'.full.html'.format(i), 'r')
        titles = re.findall('<title[^>]*>([^<]+)', artauth.read(),flags=re.MULTILINE)[0] 
        tags = ('",\n    "'.join(article.keywords))
        psource = open('HTML/'+'RiskIQ.'+(article.title).replace('?', '').replace('/', ' ')+'.full.html'.format(i), 'r')
        #print(article.canonical_link)
        title = urllib.parse.unquote(titles)
        soup = BeautifulSoup(artauth, features="lxml")
        #auth_or = 
        #author = auth_or.findAll("a", href=True)
        ref_url = [el["href"] for el in soup.findAll("a", href=re.compile("(?i)https://community.riskiq.com/projects/\d+"))]
        ref_urls = ('" "'.join(ref_url))
        author = re.search(r'By (\S+\S+\S+) \b(\S+\S+\S+)',article.html)
        #print(author.group(1), end=" "); print(author.group(2))
        #authorf = author.group(1) 
        #authorl = author.group(2)
        author_fill = (author)
        #print(author_fillz)
        article_summary = (article.summary)
        
        
    
    
        output = '''
# {}
##### Publication Date: {}
##### Authors: {}
----
{}
        '''.format(article.title, article.publish_date, author_fill, md)
        with open('Markdown/'+'RiskIQ.'+(article.title).replace('?', '').replace(':', '').replace('/', ' ').replace(',','').replace('-','').replace("'",'')+'.md0'.format(i), 'w') as f:
            f.write(output)

    except ArticleException:
        print('could not extract: {}'.format(url))

for i, url in enumerate(urls):
    try:
        article = Article(url, keep_article_html=True)
        article.download()
        article.parse()
        article.nlp()        
        guid = str(uuid.uuid4())
        brace ='{'
        newline = '\n'
        #riskiq = newspaper.build('https://www.riskiq.com/blog/labs')
        #riq_article.download()
        #riq_article.parse()
        #riq_article.nlp()
        #riskiqa = riskiq.articles
        #category = riskiq.category_urls()
        jsonoutput ='''
( 
 "title": "{}",
  "reference_urls": [
    "{}{}"
  ],
  "guid": "{}",
  "summary": "{}",
  "published_date": "{}",
  "authors": "{}",
  "featured": "False",
  "tags": [ 
    "{}"
  ],
  "indicators": ['''.format(article.title,article.url,ref_urls, guid, article_summary, article.publish_date, author_fill, tags) 

        with open('JSON.FINAL/'+'RiskIQ.'+(article.title).replace('?', '').replace(':', '').replace('/', ' ').replace(',','').replace('-','').replace("'",'')+'.json'.format(i), 'w') as f:
            import nltk_extract_and_compare_from_file_v3
            pt_ready = nltk_extract_and_compare_from_file_v3.pt_conversion(meta,fused_indicators)
            f.write(jsonoutput, json.dumps(pt_ready))

    except ArticleException:
        print('could not extract: {}'.format(url))

#for count, filename in enumerate(os.listdir(base+'/HTML')):
#    for filename in os.listdir(base+'/HTML'):
#        if not filename.startswith("."):
#            with open(os.path.join(base+ '/HTML', filename), "r") as html_reader:
#                try:
#                    for h in html_reader:
#                        phtml = INPUT_FORMAT='html'
#                        pout = OUTPUT_FORMAT='txt'
#                        ppath = PATH=open('HTML/'+'RiskIQ.'+(article.title).replace('?', '').replace('/', ' ')+'.full.html'.format(i), 'r')
#                        for r in html_reader:                     
#                            if r:
#                                t = trlparser(html, pout, ppath)
#                                print(t)      
#                except OSError:
#                    continue


for count, filename in enumerate(os.listdir(base+'/Markdown')):
    for filename in os.listdir(base+'/Markdown'):
        if not filename.startswith("."):
            with open(os.path.join(base+ '/Markdown', filename), "r") as md_reader:
                try:
                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON', Path(filename).stem +'.json'), "r")
                    filedata = f.read()
                    fix = filedata
                    #newline = line.rstrip('\r\n')
                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "a")
                    f.write(fix)
                    for i in md_reader:
                        ip_regex = '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
                        ip_finder = re.findall(ip_regex, md_reader.read())      
                        if filename.endswith(".md0"): 
                            for r in ip_finder:                     
                                if r:
                                    ip = ip_finder      
                                    #print (r)
                                    jsonoutput ='''
    (
     "type": "ip",
     "value": "{}",
     "enterprise": false
    ),'''.format(r.rstrip())

#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "a")
#                                    f.write(jsonoutput)
                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
                                    filedata = f.read()
                                    fixit = filedata.replace(r,"* ["+r+"](https://community.riskiq.com/search/"+r+")")
                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
                                    #f = open('Markdown/'+'RiskIQ.'+(article.title).replace(' ', '').replace('?', '').replace('/', ' ')+'.md0'.replace('.md0','md') .format(i), 'w')
                                    f.write(fixit)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
                                    filedata = f.read()
                                    fix = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
                                    f.write(fix)
                                    f.close()
                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md1'))
                        
                except OSError:
                    continue

for count, filename in enumerate(os.listdir(base+ '/Markdown')):
    for filename in os.listdir(base+ '/Markdown'):
        if not filename.startswith("."):
            with open(os.path.join(base+ '/Markdown', filename), "r") as md_reader:
                try:
                    for i in md_reader:    
                        dom_regex = "[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$"
                        dom_finder = re.findall(dom_regex, md_reader.read(),flags=re.MULTILINE)
                        if filename.endswith('.md1'):
                            for d in dom_finder:
                                if d:
                                    dom = dom_finder      
                                    #print (d)
                                    jsonoutput ='''
    (
     "type": "domain",
     "value": "{}",
     "enterprise": false
    ),'''.format(d)
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
#                                    f.write(jsonoutput)
                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
                                    filedata = f.read()
                                    fixit = filedata.replace(d,"* ["+d+"](https://community.riskiq.com/search/"+d+")")
                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
                                    f.write(fixit)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
                                    filedata = f.read()
                                    fix = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
                                    f.write(fix)
                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md2'))

                except OSError:
                    continue

for count, filename in enumerate(os.listdir(base+ '/Markdown')):
    for filename in os.listdir(base+ '/Markdown'):
        if not filename.startswith("."):
            with open(os.path.join(base+ '/Markdown', filename), "r") as md_reader:
                try:
                    for i in md_reader:    
                        md5_regex = "^[a-f0-9]{32}$"
                        md5_finder = re.findall(md5_regex, md_reader.read(),flags=re.MULTILINE)
                        if filename.endswith('.md2'):
                            for h in md5_finder:
                                if h:
                                    md5 = md5_finder      
                                    #print (d)
                                    jsonoutput ='''
    (
      "type": "md5",
      "value": "{}",
      "enterprise": false
    ),'''.format(h)
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
#                                    f.write(jsonoutput)
                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
                                    filedata = f.read()
                                    fixit = filedata.replace(h,("* "+h+" [Virustotal](https://www.virustotal.com/gui/search/"+h+") [ANY.RUN](https://app.any.run/submissions/#filehash:#"+h+") [Hybrid-Analysis](https://www.hybrid-analysis.com/search?query="+h+")\n"))
                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
                                    f.write(fixit)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
                                    filedata = f.read()
                                    fix = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
                                    f.write(fix)
                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md3'))
    
                except OSError:
                    continue

for count, filename in enumerate(os.listdir(base+ '/Markdown')):
    for filename in os.listdir(base+ '/Markdown'):
        if not filename.startswith("."):
            with open(os.path.join(base+ '/Markdown', filename), "r") as md_reader:
                try:
                    for i in md_reader:    
                        md5_regex = "^[a-f0-9]{40}$"
                        md5_finder = re.findall(md5_regex, md_reader.read(),flags=re.MULTILINE)
                        if filename.endswith('.md3'):
                            for h in md5_finder:
                                if h:
                                    sha = md5_finder      
                                    #print (d)
                                    jsonoutput ='''
    (
     "type": "sha1",
     "value": "{}",
     "enterprise": false
    ),'''.format(h)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
                                    f.write(jsonoutput)
                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
                                    filedata = f.read()
                                    fixit = filedata.replace(h,("* "+h+" [Virustotal](https://www.virustotal.com/gui/search/"+h+") [ANY.RUN](https://app.any.run/submissions/#filehash:#"+h+") [Hybrid-Analysis](https://www.hybrid-analysis.com/search?query="+h+")\n"))
                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
                                    f.write(fixit)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
                                    filedata = f.read()
                                    fix = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
                                    f.write(fix)
                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md4'))

                except OSError:
                    continue

for count, filename in enumerate(os.listdir(base+ '/Markdown')):
    for filename in os.listdir(base+ '/Markdown'):
        if not filename.startswith("."):
            with open(os.path.join(base+ '/Markdown', filename), "r") as md_reader:
                try:
                    for i in md_reader:    
                        md5_regex = "^[a-f0-9]{64}$"
                        md5_finder = re.findall(md5_regex, md_reader.read(),flags=re.MULTILINE)
                        if filename.endswith('.md4'):
                            for h in md5_finder:
                                if h:
                                    s256 = md5_finder      
#                                    print (d)
                                    jsonoutput ='''
    (
     "type": "sha256",
     "value": "{}",
     "enterprise": false
    ),'''.format(h)
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
#                                    f.write(jsonoutput)
                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
                                    filedata = f.read()
                                    fixit = filedata.replace(h,("* "+h+" [Virustotal](https://www.virustotal.com/gui/search/"+h+") [ANY.RUN](https://app.any.run/submissions/#filehash:#"+h+") [Hybrid-Analysis](https://www.hybrid-analysis.com/search?query="+h+")\n"))
                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
                                    f.write(fixit)
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL.FINAL', Path(filename).stem +'.json'), "r")
                                    filedata = f.read()
                                    fix = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
                                    f.write(fix)
                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md5'))
    
                except OSError:
                    continue




#for count, filename in enumerate(os.listdir(base+ '/Markdown')):
#    for filename in os.listdir(base+ '/Markdown'):
#        if not filename.startswith("."):
#            with open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r") as json_reader:
#                for last_line in json_reader:
#                    print(last_line)
#                                if h:
#                                    s256 = md5_finder      
##                                    print (d)
#                                    jsonoutput ='''
#    (
#     "type": "sha256",
#     "value": "{}",
#     "enterprise": false
#    ),'''.format(h)
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
#                                    f.write(jsonoutput)
#                                    f = open(os.path.join(base+ '/Markdown', filename), "r")
#                                    filedata = f.read()
#                                    fixit = filedata.replace(h,("* "+h+" [Virustotal](https://www.virustotal.com/gui/search/"+h+") [ANY.RUN](https://app.any.run/submissions/#filehash:#"+h+") [Hybrid-Analysis](https://www.hybrid-analysis.com/search?query="+h+")\n"))
#                                    f = open(os.path.join(base+ '/Markdown', filename),'w')
#                                    f.write(fixit)
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
#                                    filedata = f.read()
#                                    fix = filedata.replace('(','{').replace(')', '}')
#                                    #newline = line.rstrip('\r\n')
#                                    f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
#                                    f.write(fix)
#                            os.rename(os.path.join(base+ '/Markdown', filename), os.path.join(base+ '/Markdown', Path(filename).stem+'.md5'))
#    
     
#jsonend ='''
#    \n
#    ]\n
#    )
#    '''.format(d)
#f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem+'.json'), "a")
#f.write(jsonend)
#f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "r")
#filedata = f.read()
#fix = filedata.replace('(','{').replace(')', '}')
##newline = line.rstrip('\r\n')
#f = open(os.path.join('/Users/unkn0wn/Repositories/trl.article.parser/JSON.FINAL', Path(filename).stem +'.json'), "w")
#f.write(fix)
#f.close
#                                 
# %%
