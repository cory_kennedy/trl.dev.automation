#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    __ _ _   _           _           
# __|  |_| |_| |_ ___ ___| |_ _ _ ___ 
#|  |  | |  _|  _| -_|  _| . | | | . |
#|_____|_|_| |_| |___|_| |___|___|_  |
#                                |___|
#__author__    = "Cory Kennedy <cory@riskiq.net>"
#__status__ = "Development"                    

import os
import pypandoc

path = input("Enter Path to scan: ")
filetype = input("Enter Filetype [docx, txt, html, csv]: ")
results = open(filetype+".txt", "a")
    #pathy = input("Enter path to scan: ")
    #for root, dirs, files in os.walk(path):
for root, dirs, files in os.walk(path):
    for file in files:
        if file.endswith("."+filetype):
            results.write(os.path.join(root, file)+"\n")
            output = open(filetype+".txt")
            lines = [line.rstrip() for line in output]
            for line in lines:
                #filters = ['pandoc-citeproc']
                #pdoc_args = ['--mathjax',
                #             '--smart']
                mdoutput = pypandoc.convert_file(line,
                               to='md',
                               format=filetype,
                               outputfile=line+'.md')
                jsoutput = pypandoc.convert_file(line,
                               to='json',
                               format=filetype,
                               outputfile=line+'.json')
                #               #extra_args=pdoc_args,
                               #filters=filters)
                #results.write(os.path.join(root, file)+"\n")
                print(output)

                